/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.source;


import uk.nhs.interoperability.infrastructure.ITKAckDetails;
import uk.nhs.interoperability.payload.ITKMessage;
//import com.xmlsolutions.annotation.Requirement;

/**
 * Interface defining the application responsibilities
 * for managing asynchronous invocation responses - including
 * standard business responses (e.g. query responses), business
 * acknowledgements and infrastructure acknowledgements and 
 * infrastructure negative acknowledgements
 * 
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 *
 */
public interface ITKCallbackHandler {

	/**
	 * Allows the transport and distribution infrastructure
	 * to pass back an asynchronous response received in 
	 * response to an asynchronous request.<br/><br/>
	 * 
	 * The response message may be a generic ITK Business
	 * Acknowledgement (for instance as a result of a <code>sendCDA</code>
	 * invocation) or a specific business response associated
	 * with the request message.<br/><br/>
	 * 
	 * <b>Note: </b> It is the responsibility of the application to maintain
	 * state and do the necessary correlation of this message against the original
	 * request to complete the transaction.
	 * 
	 * @param response The {@link ITKMessage} representing the response
	 */
	//@Requirement(traceTo={"WS-PAT-02"})
	public abstract void onMessage(ITKMessage response);

	/**
	 * Method invoked by the transport and distribution layer
	 * upon receipt of a positive infrastructure acknowledgement (Ack).<br/><br/>
	 * 
	 * Application components are not required to provide any functionality
	 * that is triggered by this acknowledgement however it is anticipated
	 * that this could be used to implement "delivery receipt" functionality.
	 * 
	 * @param ack The (positive) acknowledgement. This can be correlated
	 * back to the request via the {@link ITKAckDetails#getTrackingRef()}
	 * method. Note correlation is an application layer responsibility.
	 */
	//@Requirement(traceTo={"WS-PAT-02"})
	public abstract void onAck(ITKAckDetails ack);

	/**
	 * Method invoked by the transport and distribution layer
	 * upon receipt of a negative infrastructure acknowledgement (Nack).<br/><br/>
	 * 
	 * Application components are not required to provide any functionality
	 * that is triggered by this acknowledgement however it is anticipated
	 * that this could be used to implement "delivery failure" functionality.
	 * 
	 * @param ack The (negative) acknowledgement. This can be correlated
	 * back to the request via the {@link ITKAckDetails#getTrackingRef()}
	 * method. Note correlation is an application layer responsibility. Details
	 * of the error associated with the "delivery failure" can be retrieved via
	 * the {@link ITKAckDetails#getNackError()} method.
	 */
	//@Requirement(traceTo={"WS-PAT-02"})
	public abstract void onNack(ITKAckDetails ack);

}