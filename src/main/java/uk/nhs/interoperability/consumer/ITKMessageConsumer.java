/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.consumer;

//import com.xmlsolutions.annotation.Requirement;
import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.payload.ITKMessage;

/**
 * API contract for between the transport and application
 * layers for a message/document consumer application. In this
 * context the message/document consumer may be the receiver of
 * a clinical correspondence message such as a discharge advice
 * or it may be the service provider in a query/response pattern
 * such as Spine Mini Services.<br/><br/>
 * 
 * It is anticipated that clinical applications will either
 * implement this interface directly - or use an an adaptor
 * that is compliant with this interface
 * 
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since version 0.1
 */
public interface ITKMessageConsumer {


	/**
	 * Call to process a request message where the application response is returned
	 * synchronously (in the same thread)
	 * 
	 * @param request The itk request message to process. Examples might include an
	 * Spine Mini Services <code>getPatientDetailsByNHSNumberRequest-v1-0</code>
	 * message
	 * 
	 * @return an <code>ITKMessage</code> representing the application/business response
	 * to the provided <code>request</code>. The response may represent business processing
	 * success or failure depending on the contents of the <code>request</code> and the
	 * implementation of the underlying service.
	 * 
	 * @throws ITKMessagingException if there are any syntactic, semantic or more general
	 * application processing issues when processing the request or constructing the response.
	 * In this context the {@link ITKMessagingException} should not be used to represent
	 * business processing failures such as a query returned no results - in general the
	 * underlying messaging specifications support "error" business response to support
	 * these types of failure.
	 */
	//@Requirement(traceTo={"WS-PAT-01"})
	public ITKMessage onSyncMessage(ITKMessage request) throws ITKMessagingException;
	
	/**
	 * Call to process a request message where no application response is returned
	 * synchronously. This may be because no application response is expected, or
	 * because the resulting response will be sent back to the <code>request</code>
	 * originator asynchronously.
	 * 
	 * @param request The itk request message to process. Examples might include an
	 * Spine Mini Services <code>nonCodedCDADocument-v2-0</code>
	 * message
	 * 
	 * @throws ITKMessagingException if there are any syntactic, semantic or more general
	 * application processing issues when processing the request or constructing the response.
	 * In general it is recommended that full syntactic and as much business level validation
	 * as practical is performed against the <code>request</code> prior to the method returning
	 * such that faults can be raised and reported back via the underlying transport.
	 *  
	 * In this context the {@link ITKMessagingException} should not be used to represent
	 * business processing failures such as a query returned no results - in general the
	 * underlying messaging specifications support "error" business response to support
	 * these types of failure.
	 */
	//@Requirement(traceTo={"WS-PAT-02"})
	public void onMessage(ITKMessage request) throws ITKMessagingException;
}
