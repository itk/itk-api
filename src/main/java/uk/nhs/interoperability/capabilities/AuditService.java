/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.capabilities;

//import com.xmlsolutions.annotation.Requirement;
import uk.nhs.interoperability.infrastructure.ITKMessageProperties;

/**
 * An representation of an ITK compliant* audit service that has the capability
 * to audit ITK messaging related events
 * 
 * In general the expected usage pattern is to audit on both the client and service
 * provider sides of any ITK operation. Additionally the intent to do something
 * should be audited - e.g. intention to send a message or call a service {@link #MESSAGE_SENT_EVENT}.
 * An additional audit call may then be required for the same logical action should
 * any processing errors occur - e.g. connection timeouts {@link #MESSAGE_PROCESSING_FAILURE}
 * 
 * <br/><br/>
 * 
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since version 0.1
 */
public interface AuditService {
	
	/**
	 * Constant representing the receipt of an ITK message
	 */
	public static final String MESSAGE_RECEIPT_EVENT = "Message received";
	
	/**
	 * Constant representing the sending of an ITK message 
	 */
	public static final String MESSAGE_SENT_EVENT = "Message sent";
	
	/**
	 * Constant representing the event of message processing failure 
	 */
	public static final String MESSAGE_PROCESSING_FAILURE = "Message processing failed";
	
	/**
	 * Writes an audit event to the audit service using the supplied information.  
	 * @param event The string description of the event - e.g.
	 * {@link #MESSAGE_PROCESSING_FAILURE}, {@link #MESSAGE_RECEIPT_EVENT}, {@link #MESSAGE_SENT_EVENT}
	 * @param eventTime The time the event occurred (in unix time). Note depending on the implementation this
	 * may differ from the time at which the audit record is actually committed
	 * @param itkMessageProperties The properties of the message to which this event pertains
	 * @throws AuditException If there are any issues accessing the underlying audit implementation
	 * (e.g. I/O exception) or if the <code>event</code> and/or <code>itkMessageProperties</code> is <code>null</code>
	 */
	//@Requirement(traceTo="IFA-SEC-02", status="abstract")
	public void auditEvent(String event, long eventTime, ITKMessageProperties itkMessageProperties) throws AuditException;

}
