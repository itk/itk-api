/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.capabilities;

//import com.xmlsolutions.annotation.Requirement;

/**
 * Interface used by ITK message recipients to determine
 * whether or not the received message is a supported
 * profile, is deprecated or is not supported.<br/><br/>
 * 
 * Note: This interface is a candidate for migration to the
 * itk-api project in due course
 * 
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since version 0.1
 */
public interface ITKProfileManager {
	
	/**
	 * The profileId is recognised and can be
	 * processed by the recipient
	 */
	public static final int ACCEPTED = 10;
	
	/**
	 * The profileId is either unknown or is
	 * not accepted by the recipient
	 */
	public static final int NOT_SUPPORTED = 20;
	
	/**
	 * The profileId is recognised and can be
	 * processed by the recipient - however support
	 * for this profile is deprecated and is likely
	 * to be removed in the future
	 */
	public static final int DEPRECATED = 30;
	
	/**
	 * Determines whether the provided <code>profileId</code>
	 * is supported.
	 * 
	 * @param profileId the profileId e.g.
	 * <code>urn:nhs-en:profile:ambulanceServicePatientReport-v1-0</code>
	 * @return an in representing the level of support for this profileId. 
	 * Permitted values of {@link #ACCEPTED}, {@link #NOT_SUPPORTED} or {@link #DEPRECATED}
	 */
	//@Requirement(traceTo={"COR-DEH-09"}, status="abstract")
	public int getProfileSupportLevel(String profileId);

}
