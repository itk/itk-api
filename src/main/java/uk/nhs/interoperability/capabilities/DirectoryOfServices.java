/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.capabilities;

import uk.nhs.interoperability.infrastructure.ITKAddress;
import uk.nhs.interoperability.service.ITKService;
import uk.nhs.interoperability.transport.ITKTransportRoute;

/**
 * An abstraction for an endpoint resolution directory.
 * Concrete implementations of this interface can be
 * simple (for instance configuration file based) or
 * be dependent on external services such as a national
 * endpoint directory - for instance
 * Spine Directory Services (SDS) 
 * 
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since version 0.1
 */
public interface DirectoryOfServices {


	/**
	 * Resolve the <code>ITKTransportRoute</code> for the supplied <code>service</code>
	 * and logical destination <code>address</code>. For non-routed messages the returned
	 * transport route will provide the physical destination address of the addressee.
	 * For routed messages the return <code>ITKTransportRoute</code> may represent the
	 * physical route to the actual destination service or it may resolve to an intermediary
	 * ITK-router - from the message originators perspective it is agnostic.
	 * 
	 * @param serviceId The serviceId representing the ITK service that is being requested
	 * e.g. <code>urn:nhs-itk:services:201005:transferPatient-v1-0</code>
	 * @param address The logical address of the destination e.g. <code>urn:nhs-uk:addressing:ods:TESTORGS:ORGB</code>
	 */
	public ITKTransportRoute resolveDestination(String serviceId, ITKAddress address);

	/**
	 * Get the <code>ITKService</code> for the supplied <code>serviceId</code>
	 * 
	 * @param serviceId The ITK Service Id representing the ITK service that is being requested
	 * e.g. <code>urn:nhs-itk:services:201005:transferPatient-v1-0</code>
	 */
	public ITKService getService(String serviceId);
}
