/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.infrastructure;

import java.util.Map;

import uk.nhs.interoperability.payload.ITKMessage;
import uk.nhs.interoperability.transport.ITKTransportProperties;

/**
 * Interface that represents the addressing, audit and other meta-data information
 * associated with the ITK Message. This interface allows these properties to be
 * passed between the application and transport layers.
 * 
 * @see ITKMessage
 * 
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since version 0.1
 * 
 */
public interface ITKMessageProperties {
	
	/**
	 * The handling specification key for the property that allows the message originator
	 * to specify whether or not they would like a business Acknowledgement.</br/><br/>
	 * 
	 * Permissible values for this handling specification are the Strings <code>true</code>
	 * or <code>false</code>
	 */
	public static final String BUSINESS_ACK_HANDLING_SPECIFICATION_KEY = "urn:nhs:itk:ns:201005:ackrequested";
	
	/**
	 * The handling specification key for the property that allows the message originator
	 * to explicitly specify the interaction id of the message.<br/><br/>
	 * 
	 * The value for this handling specification should be the interaction id of the message - 
	 * e.g. <code>urn:nhs-itk:interaction:verifyNHSNumberRequest-v1-0</code>
	 */
	public static final String INTERACTION_HANDLING_SPECIFICATION_KEY = "urn:nhs-itk:ns:201005:interaction";
	
	/**
	 * Obtains the originators ITK address
	 * @return and ITKAddress object with the address details of the message originator
	 */
	public ITKAddress getFromAddress();
	
	/**
	 * Sets the originators address for this message. 
	 * @param fromAddress the ITKAddress object with the address details of the message
	 * originator
	 */
	public void setFromAddress(ITKAddress fromAddress);

	/**
	 * Obtains the destination address for the associated ITK Message.<br/><br/>
	 * <b>Note</b> Whilst the ITK Specifications allow for multiple recipients
	 * to be specified this version of the ITK API only supports a single recipient
	 * to be defined as this satisfies the majority of anticipated use-cases.
	 * 
	 * @return The ITKAddress object containing the address details of the
	 * intended recipient
	 */
	public ITKAddress getToAddress();
	
	/**
	 * Sets the destination address for the associated ITK Message.<br/><br/>
	 * <b>Note</b> Whilst the ITK Specifications allow for multiple recipients
	 * to be specified this version of the ITK API only supports a single recipient
	 * to be defined as this satisfies the majority of anticipated use-cases.
	 * 
	 * @param toAddress the ITKAddress object containing the address details of the
	 * intended recipient
	 */
	public void setToAddress(ITKAddress toAddress);
	
	/**
	 * Returns the audit identity of the ITK message originator
	 * @return An ITKIdentity object containing the audit id
	 */
	public ITKIdentity getAuditIdentity();
	
	/**
	 * Sets the audit identity of the message originator
	 * @param An ITKIdentity object containing the audit id
	 */
	
	public void setAuditIdentity(ITKIdentity auditIdentity);

	/**
	 * ITK message service associated with the message
	 * @return a String identifying the ITK Service Id
	 */
	public String getServiceId();
	
	/**
	 * Sets the ITK message serviceId associated with this message
	 * @param serviceId The String identifying the ITK Service (from ITK Domain Message Specifications)
	 */
	public void setServiceId(String serviceId);

	/**
	 * Obtains the business payloadId associated with this
	 * message. This business payload message Id is typically
	 * used for tracking and correlating messages.<br/><br/>
	 * <b>Note</b> The business payload id is logically distinct
	 * from the {@link #getTrackingId()} as it is an id that is
	 * typically embedded somewhere in the business payload - for
	 * example as a document instance id in a CDA message, whereas
	 * the {@link #getTrackingId()} is specifically an id assigned
	 * when sending the message between two or more endpoints and
	 * should not have meaning beyond the original ITK transmission and
	 * associated acknowledgements.
	 * 
	 * @return The UUID associated with the ITK Message business
	 * payload
	 */
	public String getBusinessPayloadId();
	
	/**
	 * Sets the business payloadId associated with this
	 * message. This business payload message Id is typically
	 * used for tracking and correlating messages
	 * @param businessPayloadId The UUID associated with the
	 * ITK Message business payload
	 */
	public void setBusinessPayloadId(String businessPayloadId);

	/**
	 * Obtains the profileId associated with the ITKMessage.
	 * The profile Id represents the set of rules used to
	 * construct the message and provides a single String
	 * that aides with message/document version management as well
	 * as allowing message/document consumers to understand whether
	 * or not they are capable of handling the associated ITK Message
	 * 
	 * @return The profile id (e.g. <code>nhs-en:profile:nonCodedCDADocument-v2-0</code>)
	 */
	public String getProfileId();
	
	/**
	 * Sets the profileId associated with the ITKMessage.
	 * The profile Id represents the set of rules used to
	 * construct the message and provides a single String
	 * that aides with message/document version management as well
	 * as allowing message/document consumers to understand whether
	 * or not they are capable of handling the associated ITK Message
	 * 
	 * @param profileId The profile id (e.g. <code>nhs-en:profile:nonCodedCDADocument-v2-0</code>)
	 */
	public void setProfileId(String profileId);
	
	/**
	 * Obtains the tracking id associated with the ITKMessage
	 * when sending the message between two or more endpoints.
	 * The tracking id should not have meaning beyond the original
	 * ITK transmission and associated acknowledgements. In
	 * particular the trackingId is not expected to be an id directly
	 * represented in the business payload such as a CDA document
	 * instance id. Ids associated with the business payload are
	 * obtained via {@link #getBusinessPayloadId()}.<br/><br/>
	 * The primary purpose of the tracking Id is for operational
	 * reasons (i.e. allows entries in log file across different
	 * servers and nodes to be tied together) and for correlation
	 * of asynchronous acknowledgements such as ITK Infrastructure
	 * acknowledgements (see {@link ITKAckDetails#getTrackingRef()})
	 * and ITK Business Acknowledgements.
	 * 
	 * @return a String with the UUID represent the tracking Id
	 */
	public String getTrackingId();
	
	/**
	 * Sets the tracking id associated with the ITKMessage
	 * prior to sending the message between two or more endpoints.
	 * The tracking id should not have meaning beyond the original
	 * ITK transmission and associated acknowledgements. In
	 * particular the trackingId is not expected to be an id directly
	 * represented in the business payload such as a CDA document
	 * instance id. Ids associated with the business payload are
	 * set via {@link #setBusinessPayloadId(String)}.<br/><br/>
	 * The primary purpose of the tracking Id is for operational
	 * reasons (i.e. allows entries in log file across different
	 * servers and nodes to be tied together) and for correlation
	 * of asynchronous acknowledgements such as ITK Infrastructure
	 * acknowledgements (see {@link ITKAckDetails#getTrackingRef()})
	 * and ITK Business Acknowledgements.
	 * 
	 * @param trackingId a String UUID representing the tracking Id
	 * for this transmission. Note if the message is resent a new tracking
	 * Id should be created
	 */
	public void setTrackingId(String trackingId);
	
	/**
	 * Sets the {@link ITKTransportProperties} that are associated with the
	 * ITKMessage that was received by the service provider on the inbound
	 * leg in an asynchronous request/response invocation.<br/><br/>
	 * 
	 * The <code>inboundITKTransportProperties</code> are not expected to
	 * be processed by the application layer, however where present
	 * <b>the application layer is responsible for attaching the
	 * <code>inboundITKTransportProperties</code> to any responses generated</b>
	 * such that these transport properties are available to the transport layers
	 * transmitting the response.<br/><br/>
	 * 
	 * A typical use of this mechanism is for the propagation of the
	 * <code>&lt;wsa:replyTo/&gt;</code> URL such that it is available to the
	 * to the transport layers managing the "reply".<br/><br/>
	 * 
	 * <b>Note</b> although this pattern adds some additional burden on the
	 * application layer it offers significant advantages over the alternative
	 * where the transport layer has to maintain state and correlate messages
	 * - potentially over several distributed nodes
	 * 
	 * @param inboundITKTransportProperties The {@link ITKTransportProperties} that
	 * were obtained from the inbound message.
	 */
	public void setInboundTransportProperties(ITKTransportProperties inboundITKTransportProperties);
	
	/**
	 * Obtains the {@link ITKTransportProperties} that are associated with the
	 * ITKMessage that was received by the service provider on the inbound
	 * leg in an asynchronous request/response invocation.<br/><br/>
	 * 
	 * The <code>inboundITKTransportProperties</code> are not expected to
	 * be processed by the application layer, however where present
	 * <b>the application layer is responsible for attaching the
	 * <code>inboundITKTransportProperties</code> to any responses generated</b>
	 * such that these transport properties are available to the transport layers
	 * transmitting the response.<br/><br/>
	 * 
	 * A typical use of this mechanism is for the propagation of the
	 * <code>&lt;wsa:replyTo/&gt;</code> address such that it is available to the
	 * to the transport layers managing the "reply".<br/><br/>
	 * 
	 * <b>Note</b> although this pattern adds some additional burden on the
	 * application layer it offers significant advantages over the alternative
	 * where the transport layer has to maintain state and correlate messages
	 * - potentially over several distributed nodes
	 * 
	 * @return <code>inboundITKTransportProperties</code> - the {@link ITKTransportProperties} that
	 * were obtained from the inbound message.
	 */
	public ITKTransportProperties getInboundTransportProperties();
	
	/**
	 * Obtains the Map of the handling specifications associated with the
	 * {@link ITKMessage}.<br/><br/>
	 * 
	 * Handling specifications provide an extensible
	 * mechanism for the message originator to attach special processing instructions
	 * with the message.
	 * The current ITK specifications provide two built-in handling specifications
	 * - one for requesting a business acknowledgement message
	 * ({@link #BUSINESS_ACK_HANDLING_SPECIFICATION_KEY}), and one to explicitly
	 * associated an interaction Id with the message
	 * ({@link #INTERACTION_HANDLING_SPECIFICATION_KEY}).	 
	 * 
	 * @return The {@link Map} of all handling specifications associated
	 * with the message. For each entry in the Map contains both the key
	 * (e.g. <code>urn:nhs:itk:ns:201005:ackrequested</code>) and the
	 * associated value. If no handling specifications are associated
	 * with the message then an empty map is returned.  
	 */
	public Map<String, String> getHandlingSpecifications();
	
	/**
	 * 
	 * Allows the message originator to add a handling specification to the
	 * associated {@link ITKMessage}.<br/><br/>
	 * 
	 * Handling specifications provide an extensible
	 * mechanism for the message originator to attach special processing instructions
	 * with the message.
	 * The current ITK specifications provide two built-in handling specifications
	 * - one for requesting a business acknowledgement message
	 * ({@link #BUSINESS_ACK_HANDLING_SPECIFICATION_KEY}), and one to explicitly
	 * associated an interaction Id with the message
	 * ({@link #INTERACTION_HANDLING_SPECIFICATION_KEY}).	 
	 * 
	 * @param key The handling specification key - for instance
	 * {@link #BUSINESS_ACK_HANDLING_SPECIFICATION_KEY}
	 * 
	 * @param value The appropriate <code>value</code> for the handling specification
	 */
	public void addHandlingSpecification(String key, String value);
	
	/**
	 * Convenience method that allows the value of the handling specification
	 * to be obtained directly.
	 * 
	 * @param key The handling specification key - for instance {@link #BUSINESS_ACK_HANDLING_SPECIFICATION_KEY}
	 * @return The <code>value</code> associated with the provided key or <code>null</code> if the
	 * handling specification indicated by the key was not present
	 */
	public String getHandlingSpecification(String key);

}