/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.transport;

/**
 * Interface representing the properties of a physical
 * (transport specific) destination.
 * 
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 *
 */
public interface ITKTransportRoute {
	
	// Transport Types
	/**
	 * Indicates an unknown transport type
	 */
	public static final String UNKNOWN = "UN";
	
	/**
	 * Constant to indicate an HTTP SOAP/Web Service
	 * ITKTransportRoute
	 */
	public static final String HTTP_WS = "WS";
	
	/**
	 * Constant to indicate a DTS (Data Transfer Service)
	 * ITKTransportRoute
	 */
	public static final String DTS = "DTS";
	
	/**
	 * Constant to indicate a Spine TMS forward channel
	 * ITKTransportRoute
	 */
	public static final String SPINE_TMS = "TMS";	
	
	// Destination Types
	/**
	 * Constant to indicate that the service
	 * provider can be reached directly without
	 * any intermediary routing or the need
	 * for routing components an message instance
	 * information.
	 */
	public static final String DIRECT = "DIRECT";
	
	/**
	 * Constant to indicate that the service
	 * provider can be reached via the routed
	 * message pattern (note this does not preclude
	 * the service provider being invoked via
	 * a single message hop)
	 */
	public static final String ROUTED = "ROUTED";
	
	// Wrapper Types
	/**
	 * Constant to indicate that no wrappers are
	 * required for the transport route
	 */
	public static final String NO_WRAPPER = "NONE";
	
	/**
	 * Constant to indicate that a distribution envelope
	 * should wrap the business payload for the transport
	 * route. 
	 * 
	 * <br/><br/>Any additional wrappers such as
	 * <code>&lt;SOAP/&gt;</code> wrappers are implied and
	 * handled via the transport specific components
	 */
	public static final String DISTRIBUTION_ENVELOPE = "DE";
	
	/**
	 * Obtains the transport type for this
	 * ITKTransportRoute instance
	 * 
	 * @return the transport type. Can be one of
	 * 
	 * {@link ITKTransportRoute#DTS}; {@link ITKTransportRoute#HTTP_WS};
	 * {@link ITKTransportRoute#SPINE_TMS} or {@link ITKTransportRoute#UNKNOWN}
	 */
	public String getTransportType();
	
	/**
	 * Obtains the transport specific physical
	 * destination address
	 * 
	 * @return A string representation of the
	 * transport specific destination address 
	 * for this ITKTransportRoute
	 */
	public String getPhysicalAddress();

	/**
	 * Obtains the address for any asynchronous replies
	 * 
	 * @return the address for any asynchronous replies
	 */
	public String getReplyToAddress();
	
	/**
	 * Obtains the address for sending an exceptions
	 * that are reported asynchronously
	 * 
	 * @return the address for asynchronous exceptions
	 */
	public String getExceptionToAddress(); 
	
	/**
	 * Obtains the destination type
	 * 
	 * @return the destination type - can be one
	 * of {@link #DIRECT} or {@link #ROUTED}
	 */
	public String getDestinationType();
	
	/**
	 * Obtains the details of any transport route
	 * wrappers that are required for the business
	 * payload
	 * 
	 * @return The wrapper type - can be one of
	 * {@link #NO_WRAPPER} or {@link #DISTRIBUTION_ENVELOPE}
	 */
	public String getWrapperType();
	
	/**
	 * Sets the details of any transport route
	 * wrappers that are required for the business
	 * payload
	 * 
	 * @param wrapperType The wrapper type - can be one of
	 * {@link #NO_WRAPPER} or {@link #DISTRIBUTION_ENVELOPE}
	 */
	public void setWrapperType(String wrapperType);
	
	/**
	 * Obtains the time to live for any messages
	 * being sent via this ITKTransportRoute
	 * 
	 * @return The time to live (in Seconds)
	 */
	public int getTimeToLive();
}
