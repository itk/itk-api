/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payload;

import uk.nhs.interoperability.capabilities.DirectoryOfServices;
import uk.nhs.interoperability.infrastructure.ITKMessageProperties;
import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.transport.ITKTransportRoute;

/**
 * Interface representing the characteristics
 * of an ITK message being transmitted via the
 * ITK message transport and distribution layers (i.e. this API).
 * <br/><br/>
 * 
 * As well as providing a standard way of setting / obtaining
 * the business payload (i.e. the business message itself) it 
 * also provides a standardised set of meta-data for the message
 * too via the {@link ITKMessageProperties}. <br/><br/>
 * 
 * <b>Note:</b> it is expected that business payload API developers will
 * either provide classes that directly implement this interface or
 * will provide suitable adaptor classes to enable the payload and
 * transport distribution APIs to be seamlessly integrated.
 * 
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since version 0.1
 */
public interface ITKMessage {

	/**
	 * Obtains the {@link ITKMessageProperties} associated with this
	 * message. These properties provide relevant addressing and meta-data
	 * about the message.
	 * 
	 * @return the ITKMessageProperties associated with the message or
	 * <code>null</code> if no ITKMessageProprties have been set.
	 */
	public ITKMessageProperties getMessageProperties();
	
	/**
	 * Sets the {@link ITKMessageProperties} associated with this
	 * message. These properties provide relevant addressing and meta-data
	 * about the message.
	 * 
	 * @param itkMessageProperties the ITKMessageProperties associated with the message.
	 */
	public void setMessageProperties(ITKMessageProperties itkMessageProperties);
	
	/**
	 * Sets the business payload portion of the message to be transmitted via
	 * the ITK transport and distribution infrastructure.<br/><br/>
	 * 
	 * <b>Note:</b> those (ADT v2 pipe and hat) messages that ITK requires
	 * to be base64 encoded when being transmitted must not need to be pre-encoded - i.e.
	 * they should be supplied in plain text. It is the responsibility of the implementation
	 * to ensure that appropriate encoding/decoding is performed behind the scenes.<br/><br/>
	 * 
	 * <b>Note: </b> for version 0.1 the signature of this method has been defined as a String
	 * to keep implementation simple. As a consequence implementations should ensure that payloads
	 * are provided as UTF-16 encoded strings. 
	 *  
	 * @param businessPayload A string representation of the business payload message
	 */
	public void setBusinessPayload(String businessPayload);
	
	/**
	 * Obtains the business payload portion of the message to be transmitted via
	 * the ITK transport and distribution infrastructure.<br/><br/>
	 * 
	 * <b>Note:</b> those (ADT v2 pipe and hat) messages that ITK requires
	 * to be base64 encoded when being transmitted will not be encoded  - i.e.
	 * they will be supplied in plain text. It is the responsibility of the implementation
	 * to ensure that appropriate encoding/decoding is performed behind the scenes.<br/><br/>
	 * 
	 * @return for version 0.1 the return type of this method has been defined as a String
	 * to keep implementation simple. As a consequence the business payload will be returned as a
	 * UTF-16 encoded string
	 */
	public String getBusinessPayload();
	
	/**
	 * Obtains the full ITKMessage including any wrappers
	 * 
	 * @return The full ITK message payload including wrappers. 
	 * For ITKMessage instances that includes wrappers (for instance where the
	 * business payload has been wrapped in a distribution envelope or SOAP
	 * envelope) this method returns the full message. Otherwise the method
	 * returns an identical String to {@link #getBusinessPayload()}
	 * 
	 * @throws ITKMessagingException If there is an error serialising the
	 * full ITKMessage
	 */
	public String getFullPayload() throws ITKMessagingException;
	
	/**
	 * Obtains any pre-resolved {@link ITKTransportRoute} associated
	 * with this ITKMessage.<br/><br/>
	 * 
	 * A pre-resolved transport route might be present if this
	 * ITKMessage instance is a response where the return address
	 * is already known (e.g. via a SOAP <code>&lt;wsa:ReplyTo/&gt;</code>
	 * address provided in the request), or because the application layer
	 * has already resolved the {@link ITKTransportRoute} for the target
	 * destination.
	 * 
	 * @return a pre-resolved {@link ITKTransportRoute} if present, or 
	 * <code>null</code> otherwise. If no pre-resolved transport route is
	 * present it is the responsibility of the implementation to determine
	 * the {@link ITKTransportRoute} via the
	 * {@link DirectoryOfServices#resolveDestination(String, uk.nhs.interoperability.infrastructure.ITKAddress)}
	 * method call
	 */
	public ITKTransportRoute getPreresolvedRoute();
	
	/**
	 * Indicates whether this ITKMessage instance is
	 * in response to a request message such as a query
	 * 
	 * @return <code>true</code> if this message is a
	 * response, <code>false</code> otherwise.
	 */
	public boolean isResponse();

}